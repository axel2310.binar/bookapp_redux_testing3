import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { GET_BOOKS_API } from '../../../src/config';

const getRecommendedApi = async (token, limit) => {
  try {
    return await axios.get(GET_BOOKS_API, {
      headers: {
        Authorization: `Bearer ${token}`, params: { limit },
      },
    });
  } catch (e) {
    return [];
  }
};

describe('GET_BOOKS_API', () => {
  let mock;

  beforeAll(() => {
    mock = new MockAdapter(axios);
  });

  afterEach(() => {
    mock.reset();
  });

  describe('getRecommendedApi', () => {
    it('should return an object', async () => {
      const response = {
        results: [
          {
            title: 'Harry Potter and the Order of the Phoenix',
            author: 'J. K. Rowling',
            cover_image: 'https://images-na.ssl-images-amazon.com/images/I/51SfTd37PaL._SX415_BO1,204,203,200_.jpg',
            id: '6231453513c01e6f8b566ece',
            publisher: 'Scholastic',
            average_rating: '8',
            price: '292532',
          },
        ],
      };
      mock.onGet(GET_BOOKS_API).reply(200, response);
      const result = await getRecommendedApi('token', 1);
      expect(result.data).toEqual(response);
    });

    it('should return an empty object', async () => {
      mock.onGet(GET_BOOKS_API).reply(400, []);
      const result = await getRecommendedApi('token', 1);
      expect(result).toEqual([]);
    });
  });
});
