import ReduxThunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store';
import axios from 'axios';
import { register, registerLoading, signupUser } from '../../../src/redux/action';

jest.mock('react-native/Libraries/EventEmitter/NativeEventEmitter');
jest.mock('axios');

describe('Register Action Test', () => {
  it('Register Action Test', () => {
    expect(register()).toEqual({
      type: '@REGISTER',
    });
    expect(registerLoading(true)).toEqual({
      type: '@REGISTER_LOADING',
      payload: true,
    });
  });
});

describe('Register User Test', () => {
  const middlewares = [ReduxThunk];
  const mockStore = configureMockStore(middlewares);
  const store = mockStore({});

  // Register Success
  test('should Register a  user ', async () => {
    axios.post.mockImplementation(() => Promise.resolve({
      status: 200,
    }));

    const testUser = {
      name: 'testName',
      email: 'test@email.com',
      password: 'testPassword',
    };

    await store.dispatch(signupUser(testUser)).then(() => {
      expect(store.getActions()[0]).toEqual(
        registerLoading(true),
        register(),
      );
    });
  });

  // Register fail
  test('should not Register a  user', async () => {
    axios.mockRejectedValue({
      status: 500,
    });

    const testUser = {
      name: '',
      email: '',
      password: '',
    };

    await store.dispatch(signupUser(testUser)).then(() => {
      expect(store.getActions()[0]).toEqual(
        registerLoading(true),
        registerLoading(false),
      );
    });
  });
});
