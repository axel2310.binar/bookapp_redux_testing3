import { AverageRatingSort } from '../../src/utils/helperFunction';

jest.mock('react-native/Libraries/EventEmitter/NativeEventEmitter');

describe('AverageRatingSort Test', () => {
  test('AverageRatingSort Test', () => {
    const testData = [
      {
        average_rating: '4.5',
      },
      {
        average_rating: '4.0',
      },
      {
        average_rating: '3.5',
      },
      {
        average_rating: '3.0',
      },
      {
        average_rating: '2.5',
      },
      {
        average_rating: '2.0',
      },
      {
        average_rating: '1.5',
      },
      {
        average_rating: '1.0',
      },
      {
        average_rating: '0.5',
      },
      {
        average_rating: '0.0',
      },
    ];
    const result = testData.sort(AverageRatingSort);
    expect(result).toEqual([
      {
        average_rating: '4.5',
      },
      {
        average_rating: '4.0',
      },
      {
        average_rating: '3.5',
      },
      {
        average_rating: '3.0',
      },
      {
        average_rating: '2.5',
      },
      {
        average_rating: '2.0',
      },
      {
        average_rating: '1.5',
      },
      {
        average_rating: '1.0',
      },
      {
        average_rating: '0.5',
      },
      {
        average_rating: '0.0',
      },

    ]);
  });
});
