import BookDetailScreen from './BookDetailScreen';
import HomeTabScreen from './HomeTabScreen';
import LoginScreen from './LoginScreen';
import RegisterScreen from './RegisterScreen';
import SplashScreen from './SplashScreen';
import SuccessRegisterScreen from './SuccessRegisterScreen';

export {
  BookDetailScreen, HomeTabScreen, LoginScreen, RegisterScreen, SplashScreen, SuccessRegisterScreen,
};
