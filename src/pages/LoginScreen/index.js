import React, { useState } from 'react';
import {
  ActivityIndicator,
  Dimensions,
  Image,
  KeyboardAvoidingView,
  StatusBar,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { HeaderLogin } from '../../assets';
import { ButtonComponent, Input, LinkComponent } from '../../component';
import { loginUser } from '../../redux';
import { colors, fonts } from '../../utils';

function LoginScreen({ navigation }) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const dispatch = useDispatch();
  const isLoading = useSelector((state) => state.Auth.isLoading);

  const onSubmit = () => {
    dispatch(loginUser(email, password, navigation));
  };

  const HandleEmail = (text) => {
    setEmail(text);
  };

  const HandlePassword = (text) => {
    setPassword(text);
  };

  return (
    <KeyboardAvoidingView
      behavior="padding"
      keyboardVerticalOffset={40}
      style={styles.page}
    >
      <StatusBar
        barStyle="dark-content"
        backgroundColor={colors.background.primary}
      />
      <View style={{ alignItems: 'center' }}>
        <View style={styles.header}>
          <Image style={styles.image} source={HeaderLogin} />
        </View>
      </View>
      <Input
        placeholder="Email"
        marginTopInput={20}
        keyboardType="email-address"
        value={email}
        onChangeText={(text) => HandleEmail(text)}
        testID="input-email"
      />
      <Input
        placeholder="Password"
        value={password}
        onChangeText={(text) => HandlePassword(text)}
        marginTopInput={10}
        marginBottomInput={10}
        secureTextEntry
        testID="input-password"
      />
      <ButtonComponent
        title={isLoading ? <ActivityIndicator /> : 'Login'}
        onPress={() => onSubmit()}
        testID="button-login"
      />
      <Text style={styles.text}>Dont have an account</Text>
      <LinkComponent
        title="Register"
        size={16}
        align="center"
        onPress={() => navigation.navigate('RegisterScreen')}
        testID="button-register"
      />
    </KeyboardAvoidingView>
  );
}

export default LoginScreen;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: colors.background.primary,
    paddingHorizontal: 13,
    paddingVertical: 13,
  },
  image: { height: null, width: null, flex: 1 },
  header: {
    height: windowHeight * 0.35,
    width: windowWidth,
    paddingHorizontal: 13,
  },
  text: {
    textAlign: 'center',
    marginTop: 13,
    fontFamily: fonts.primary[600],
    color: colors.text.secondary,
  },

  button: {
    backgroundColor: colors.background.secondary,
    borderRadius: 10,
    paddingVertical: 10,
  },
});
