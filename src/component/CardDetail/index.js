import React from 'react';
import {
  Dimensions, Image, StyleSheet, Text, View,
} from 'react-native';
import { colors, fonts } from '../../utils';
import ButtonComponent from '../ButtonComponent';

function CardDetail({
  title, author, publisher, source, pdf, video, sound,
}) {
  return (
    <View style={styles.card}>
      <Image source={source} style={styles.image} />
      <View style={styles.detailBook}>
        <Text style={styles.title}>{title}</Text>
        <Text style={styles.author}>
          Author by :
          {' '}
          {author}
        </Text>
        <Text style={styles.publisher}>
          Publisher by :
          {' '}
          {publisher}
        </Text>
        <ButtonComponent title="Pdf" onPress={pdf} mediaHandling />
        <ButtonComponent title="Video" onPress={video} mediaHandling />
        <ButtonComponent title="Sound" onPress={sound} mediaHandling />
      </View>
    </View>
  );
}

export default CardDetail;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  card: {
    width: windowWidth * 0.90,
    height: windowHeight * 0.32,
    backgroundColor: colors.background.primary,
    alignSelf: 'center',
    borderRadius: 10,
    paddingVertical: 12,
    paddingHorizontal: 8,
    flexDirection: 'row',
    elevation: 35,
  },

  image: {
    height: '100%',
    width: null,
    flex: 3,
    marginRight: 10,
    borderRadius: 4,
  },

  detailBook: {
    flex: 5,
  },

  title: {
    fontSize: 18,
    color: colors.text.primary,
    fontFamily: fonts.primary[700],
  },

  author: {
    fontSize: 13,
    color: colors.text.secondary,
    fontFamily: fonts.primary[600],
    paddingTop: 5,
  },

  publisher: {
    fontSize: 13,
    color: colors.text.secondary,
    fontFamily: fonts.primary[600],
    paddingTop: 5,

  },

});
