import React from 'react';
import {
  StyleSheet, TouchableOpacity, View, Dimensions, Image, Text,
} from 'react-native';
import { Rating } from 'react-native-elements';
import { colors, fonts } from '../../utils';

function Card({
  onPress, source, judul, author, publisher, harga, rating,
}) {
  const hargaConvert = `Rp. ${parseFloat(harga).toLocaleString('id-ID')}`;
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={onPress} style={{ height: '65%', width: '100%' }}>
        <Image style={styles.image} source={source} />
      </TouchableOpacity>
      <Text numberOfLines={1} style={styles.judul}>{judul}</Text>
      <View style={styles.wrapper}>
        <Text numberOfLines={1} style={styles.author}>
          Author:
          {' '}
          {author}
        </Text>
      </View>
      <Text numberOfLines={1} style={styles.publisher}>
        Publish :
        {' '}
        {publisher}
      </Text>
      <View style={styles.wrapper}>
        <Text numberOfLines={1} style={styles.harga}>
          {
            hargaConvert
          }
        </Text>
      </View>
      <View style={styles.wrapper}>
        <Rating
          imageSize={12}
          readOnly
          fraction={2}
          startingValue={rating / 2}

        />
      </View>
    </View>
  );
}

export default Card;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    marginTop: 10,
    overflow: 'hidden',
    height: windowHeight * 0.40,
    width: windowWidth * 0.29,
    borderRadius: 4,
  },
  image: {
    height: '100%',
    width: '100%',
  },
  judul: {
    fontFamily: fonts.primary[600],
    fontSize: 13,
    color: colors.text.primary,
  },

  author: {
    fontSize: 11,
    color: colors.text.secondary,
    fontFamily: fonts.primary.normal,
    paddingRight: 2,
  },

  publisher: {
    fontFamily: fonts.primary.normal,
    fontSize: 11,
    color: colors.text.secondary,
  },

  harga: {
    fontFamily: fonts.primary[600],
    color: colors.text.rating,
    fontSize: 11,
  },
  rating: {
    fontFamily: fonts.primary[600],
    color: colors.text.primary,
    fontSize: 14,
    width: '50%',
  },
  wrapper: {
    flexDirection: 'row', justifyContent: 'space-between',
  },

});
