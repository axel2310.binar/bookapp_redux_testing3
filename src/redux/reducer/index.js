import {
  GET_BOOKS_ID,
  GET_BOOKS_POPULAR,
  GET_BOOKS_RECOMMENDED,
  LOGIN, LOGIN_LOADING, LOGOUT,
  REGISTER,
  REGISTER_LOADING,
  SET_LOADING, SET_ONLINE, SET_REFRESHING,
} from '../types';

const initialBookState = {
  booksRecommended: [],
  booksPopular: [],
  isLoading: false,
  isOnline: true,
  booksId: [],
  isRefreshing: false,
};

const initialAuthState = {
  token: null,
  name: '',
  isLoading: false,
};

// eslint-disable-next-line default-param-last
export const dataReducers = (state = initialBookState, action) => {
  switch (action.type) {
    case GET_BOOKS_RECOMMENDED:
      return {
        ...state,
        booksRecommended: action.payload,
        isLoading: false,
        isRefreshing: false,
      };
    case GET_BOOKS_POPULAR:
      return {
        ...state,
        booksPopular: action.payload,
        isLoading: false,
        isRefreshing: false,
      };
    case GET_BOOKS_ID:
      return {
        ...state,
        booksId: action.payload,
        isLoading: false,
        isRefreshing: false,
      };

    case SET_LOADING:
      return {
        ...state,
        isLoading: action.payload,
      };
    case SET_ONLINE:
      return {
        ...state,
        isOnline: action.payload,
      };

    case SET_REFRESHING:
      return {
        ...state,
        isRefreshing: action.payload,
      };

    default:
      return state;
  }
};

// eslint-disable-next-line default-param-last
export const authReducers = (state = initialAuthState, action) => {
  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        token: action.payload,
        name: action.name,
        isLoading: false,
      };
    case LOGIN_LOADING:
      return {
        ...state,
        isLoading: action.payload,
      };

    case LOGOUT:
      return {
        ...state,
        token: null,
        name: '',
      };

    case REGISTER:
      return {
        ...state,
        isLoading: false,
      };

    case REGISTER_LOADING:
      return {
        ...state,
        isLoading: action.payload,
      };

    default:
      return state;
  }
};
